/*-----------------------------------------------------------------------------
    Name: exit_script
    Generated By: netstorm
    Date of generation: 03/14/2022 02:20:04
    Flow details:
    Build details: 4.8.0 (build# 59)
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

int exit_script()
{
    return 0;
}
